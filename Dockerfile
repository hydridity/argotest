FROM python:3.7

COPY src ./app

WORKDIR /app

ENV PYTHONUNBUFFERED=1

CMD ["python", "test.py"]
